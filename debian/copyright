Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: leela-zero
Source: https://github.com/gcp/leela-zero

Files: *
Copyright:
 2017-2018 Gian-Carlo Pascutto <gcp@sjeng.org>
 2017-2018 Marco Calignano
 2017-2018 Leela Zero contributors
 2017-2018 Michael O
 2017-2018 Seth Troisi
 2018 Junhee Yoo
 2017 Henrik Forsten
 2016 Ilari Pihlajisto
License: GPL-3+

Files: scripts/cpplint.py
Copyright: 2009 Google Inc.
License: BSD-3-clause

Files: src/ThreadPool.h
Copyright:
 2012      Jakob Progsch, Václav Zeman
 2017-2018 Gian-Carlo Pascutto <gcp@sjeng.org>
 2017-2018 Leela Zero contributors
License: Zlib

Files: src/half/half.hpp
Copyright: 2012-2017 Christian Rau <rauy@users.sourceforge.net>
License: Expat

Files: src/clblast_level3/*
       src/clblast_level3_half/*
Copyright: 2015-2018 Cedric Nugteren <www.cedricnugteren.nl>
License: Apache-2.0
Comment: A few embedded files originally from https://github.com/CNugteren/CLBlast

Files: ./src/CL/cl2.hpp
Copyright: 2008-2016 The Khronos Group Inc
License: Expat-plus-Khronos-notice
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS
 KHRONOS STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS
 SPECIFICATIONS AND HEADER INFORMATION ARE LOCATED AT
    https://www.khronos.org/registry/
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

Files: debian/*
Copyright: 2018 Ximin Luo <infinity0@debian.org>
License: GPL-3+

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: BSD-3-clause
 Debian systems provide the BSD 3-clause license in
 /usr/share/common-licenses/BSD replacing references to the Regents of the
 University of California with the appropriate copyright holder.

License: GPL-3
 Debian systems provide the GPL 3 license in
 /usr/share/common-licenses/GPL-3

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.
